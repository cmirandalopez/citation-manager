import cmd
import pymongo
from pymongo import MongoClient
from bson import BSON, SON
import pprint

client = MongoClient('localhost', 27017)

class BookmarksCmd(cmd.Cmd):
    
    def __init__(self, documents, tags):
        super(BookmarksCmd, self).__init__()
        self.documents = documents
        self.tags = tags
        self.prompt = "bookmarks> "

        self.tags_cache = [tag['_id'] for tag in tags.find()]

    def print_(self, doc):
            print("---" * 10)
            print(f"Tags: {doc['tags']}")
            print(doc['title'])
            print()
            print(doc['abstract'][:500])
            print()
            print(doc['doclink'])
            print()
            print("References:")
            for i, ref in enumerate(doc['references']):
                print(f"[{i + 1}] {ref}")
            print()

    def lookup_(self, kind, text):

        print(f"Results of {kind} = {text}\n")

        if kind in ["tag", "tags"]:
            tags = text
            docs = self.documents.find({'tags': {"$in": tags}})
            for doc in docs[:10]:
                self.print_(doc)
        elif kind == "title":
            docs = self.documents.find({'title': ' '.join(text)})
            for doc in docs[:10]:
                self.print_(doc)
        elif kind == "content":
            content = ' '.join(text)
            docs = self.documents.find({"$text": {"$search": content}})
            for doc in docs[:10]:
                self.print_(doc)

    def do_lookup(self, line):
        line = line.split()
        if len(line) == 0:
            return self.help_lookup()
        elif len(line) == 1:
            return self.help_lookup(line[0])
        elif len(line) > 1:
            kind, text = line[0], line[1:]
            return self.lookup_(kind, text)
    
    def complete_lookup(self, text, line, start_index, end_index):
        if text:
            return [tag for tag in self.tags_cache if tag.startswith(text)]
        else:
            return self.tags_cache
    
    def help_lookup(self, kind=None):
        if kind in ["tag", "title", "content"]:
            print(f"Usage: lookup {kind} <lookup string>")
        else:
            print("Usage: lookup <tag|title|content> <lookup string>")
    
    def add_document_(self):
        title, content, doclink, ref, tag = "", "", "", "_", "_"
        refs, tags = [], []
        while len(title) == 0:
            title = input("Title: ")

        while len(content) == 0:
            print("Abstract (empty line to finish): ")
            lines = []
            line = "_"
            while True:
                line = input()
                if len(line) > 0:
                    lines.append(line)
                else:
                    break
            content = '\n'.join(lines)

        while len(doclink) == 0:
            doclink = input("Document link: ")
        
        while len(ref) > 0:
            ref = input(f"Reference {len(refs) + 1}: ")
            if len(ref) > 0:
                refs.append(ref)
            else:
                break
        
        while len(tag) > 0:
            tag = input(f"Tag {len(tags) + 1}: ")
            if tag in self.tags_cache:
                tags.append(tag)
            elif len(tag) == 0:
                break
            else:
                suggestions = [ttag for ttag in self.tags_cache if ttag.startswith(tag)]
                print(f"Tag suggestions {sorted(suggestions)}")
        self.documents.insert_one({'title':title, 'abstract':content, 'doclink':doclink, 'references':refs, 'tags':tags})
        return "Document added."

    def add_tag_(self, tags):
        if len(tags) > 1:
            concat = input('Multiple tags detected. Concatenate? [Y/n]')
            if concat:
                tags = '-'.join(tags)

        # (name, parent) pairs
        tags = [(b[1], b[0]) for b in map(lambda x: x.split('>'), tags)]

        if len(tags) > 1:
            self.tags.insert_many([{'_id': name, 'parent': parent} for name, parent in tags])
        else:
            tags = tags[0]
            self.tags.insert_one({'_id': tags[0], 'parent': tags[1]})
        
        self.tags_cache.extend([tag[0] for tag in tags])

    def do_add(self, line):
        line = line.split()
        if len(line) == 0:
            return self.help_add()
        elif len(line) == 1:
            return self.help_lookup(line[0])
        elif len(line) > 1:
            kind, text = line[0], line[1:]
            if kind in ['doc', 'document']:
                self.add_document_()
            elif kind in ['tag', 'tags']:
                self.add_tag_(line[1:])
            else:
                return self.help_add()
    
    def do_exit(self, line):
        return True
        
    def help_add(self):
        return "Usage: add <document|tag> <string>"

if __name__ == "__main__":

    wiki = client.wiki
    documents = wiki.documents
    tags = wiki.tags

    mcmd = BookmarksCmd(documents=documents, tags=tags)
    mcmd.cmdloop()