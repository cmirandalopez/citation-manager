import pymongo
from pymongo import MongoClient
from bson import BSON, SON
import pprint

client = MongoClient('localhost', 27017)

if 'wiki' in client.list_database_names():
    remove = input("Wiki database already exists. Remove? [Y/n]").lower()
    if remove == 'y':
        client.drop_database('wiki')
else:
    print("Wiki database does not exist. Creating...")

wiki = client.wiki

for collection in ['documents', 'tags']:
    if collection in wiki.list_collection_names():
        remove = input(f"{collection} collection already exists. Remove? [Y/n]").lower()
        if remove == 'y':
            client.wiki[collection].drop()
    else:
        print(f"{collection} collection does not exist. Creating...")

documents = wiki.documents
documents.create_index([("notes", pymongo.TEXT)])
tags = wiki.tags

print("Adding a document...")
document_id = documents.insert_one({'title':'Title 1', 'notes':'Notes ', 'doclink':"https://lol.com", 'references':['https://lols.com'], 'tags':['machine-learning', 'imbalanced-learning']}).inserted_id

print("Adding many documents...")
documents.insert_many([
    {'title':'Title 2', 'notes':'Notes ', 'doclink':"https://lol.com", 'references':['https://lols.com'], 'tags':['imbalanced-learning']},
    {'title':'Title 3', 'notes':'Notes ', 'doclink':"https://lol.com", 'references':['https://lols.com'], 'tags':['machine-learning']},
    {'title':'Title 4', 'notes':'Notes ', 'doclink':"https://lol.com", 'references':['https://lols.com'], 'tags':['imbalanced-learning, machine-learning']},
    {'title':'Title 4', 'notes':'Notes ', 'doclink':"https://lol.com", 'references':['https://lols.com'], 'tags':['imbalanced-learning, machine-learning']}
])

print("Adding tags")
tags.insert([{'_id': 'machine learning', 'parent':None}, {'_id': 'imbalanced learning', 'parent':'machine learning'}])

#print("Removing duplicates")


print("Single result")
pprint.pprint(documents.find_one({'tags':['machine-learning']}))

print("Multiple results")
docs = documents.find({'tags': {"$in": ['machine-learning']}})
for doc in docs:
    pprint.pprint(doc)

print("Classic aggregation")
pipeline = [
    {"$unwind": "$tags"},
    {"$group": {"_id": "$tags", "count": {"$sum": 1}}},
    {"$sort": SON([("count", -1), ("_id", -1)])}
]
pprint.pprint(list(documents.aggregate(pipeline)))

print("Map/Reduce aggregation")
from bson.code import Code

mapper = Code("""
                function () {
                    this.tags.forEach(function (z) {
                        emit(z, 1);
                    });
                }
""")

reducer = Code("""
                function (key, values) {
                    var total = 0;
                    for (var i = 0; i < values.length; i++) {
                        total += values[i];
                    }
                    return total;
                }
""")

result = documents.map_reduce(mapper, reducer, "mr_results") # Save to a particular db
for doc in result.find():
    pprint.pprint(doc)