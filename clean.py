from pymongo import MongoClient

client = MongoClient('localhost', 27017)
clean = input('Drop wiki database? [Y/n] ').lower()
if clean == 'y':
    client.drop_database('wiki')
else:
    clean = input('Drop documents collection? [Y/n] ').lower()
    if clean == 'y':
        client.wiki.documents.drop()

    clean = input('Drop tags collection? [Y/n] ').lower()
    if clean == 'y':
        client.wiki.tags.drop()

print('Done cleaning.')